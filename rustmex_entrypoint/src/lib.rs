use proc_macro::{TokenStream};

use quote::quote;
use syn::ItemFn;

#[proc_macro_attribute]
pub fn entrypoint(_attr: TokenStream, mut item: TokenStream) -> TokenStream {
	let input: ItemFn = syn::parse(item.clone()).unwrap();
	let name = input.sig.ident;

	let gen = quote! {
		#[no_mangle]
		pub extern "C" fn mexFunction(nlhs: ::std::os::raw::c_int, lhs: *mut *mut ::rustmex::mxArray,
						nrhs: ::std::os::raw::c_int, rhs: *const *const ::rustmex::mxArray) {

			// SAFETY: it is UB to unwind over FFI boundaries; we must
			// therefore catch an unwinding panic here.
			match std::panic::catch_unwind(|| {
				let rhslice = unsafe { ::std::slice::from_raw_parts(rhs as *const &::rustmex::mxArray, nrhs as usize) };
				let lhslice = unsafe { ::std::slice::from_raw_parts_mut(lhs as *mut Option<&mut ::rustmex::mxArray>, nlhs as usize) };

				// SAFETY: Transmuting to an MxArray is safe, since both it and
				// NonNull are repr(transparent).
				//
				// Also NOTE that transmute does nothing if Lhs is from before
				// v0.1.3, since where then doing transmute<T, T>.
				let lhslice_owned = unsafe { ::std::mem::transmute(lhslice) };

				// SAFETY: LhsAns is a type safe to use _if and only if_
				// the slice it was constructed with is Matlab's Lhs
				// slice, i.e. even if Matlab passes in nlhs = 0, there
				// is still space allocated to store _1_ MxArray
				let lhs_ans = unsafe { ::rustmex::LhsAns::new(lhslice_owned) };

				// SAFETY: Transmuting from LhsAns to Lhs is safe because
				// LhsAns is repr(transparent). If used with a different
				// version of rustmex, this is a no-op.
				let lhs_ans2 = unsafe { ::std::mem::transmute(lhs_ans) };

				#name(lhs_ans2, rhslice)
			}) {
				Ok(Ok(_)) => return,
				// SAFETY: We also can't trigger a Matlab exception
				// (because that's apparently what it is) from within the
				// catch_unwind closure, so we have to smuggle our error
				// out too.
				Ok(Err(e)) => ::rustmex::trigger_error!(e),
				Err(_) => {
					::rustmex::trigger_error!(::rustmex::message::AdHoc(
						"rustmex:panic",
						"The MEX function you called paniced. Consider restarting your interpreter with an appropriate value of RUST_BACKTRACE (setenv does not seem to work)"
					));
				}
			}
		}
	};
	let entrypoint: TokenStream = gen.into();
	item.extend(entrypoint);
	item
}
