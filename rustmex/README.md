# Rustmex
A library providing convenient Rust bindings to Matlab's MEX C api.

Rustmex makes writing MEX functions in Rust a bit easier. It convert Matlab types, and
the arguments it provides to `mexFunction` into more Rusty types, which can then be used
to interface with other Rust code easily.

For installation and documentation & linkage examples, please refer to the documentation.

## v0.6 Update Notes
Rustmex v0.6 constitutes a major redesign of the crate. It has been broken up into a set
of crates, such that each crate is compilable separately; feature flags are no longer
required for Rustmex itself to compile. However, the easiest installation method is still
to use feature flags to select the appropriate backend; confer to the documentation of
the backend module for instructions. The 'normal' user facing API of Rustmex should
however still mostly be the same as before; only the internals have changed.

## Usage Examples

Below is a 'Hello World' with Rustmex:
```rust,ignore
use rustmex::prelude::*;

#[rustmex::entrypoint]
fn hello_world(lhs: Lhs, rhs: Rhs) -> rustmex::Result<()> {
	println!("Hello Matlab!");
	Ok(())
}
```
It shows the basic parts of a Rustmex MEX file: a function annotated as the `entrypoint`,
the `Lhs` and `Rhs` arrays, being the return values and arguments respectfully, and the
`Result` type to indicate error conditions. More examples (which are tested) can be found
in the `examples` directory in the Rustmex repository.

## Licence
This is licensed to you under the Mozilla Public License, version 2. You can the licence
in the LICENCE file in this project's source tree root folder.

## Authors
- Niels ter Meer (maintainer)
