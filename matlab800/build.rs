
fn main() -> Result<(), &'static str> {

	// Only fail the build on platforms+target_apis where we know the build would
	// have failed with a linker error otherwise.
	if cfg!(target_os = "windows") && cfg!(not(feature = "octave")) {
		Err(
		"This script should never run!\
		\
		Rustmex cannot guess where your Matlab installation is located.\
		Since it needs to link to libmx, libmex, and libmat, you need to tell\
		the linker where they are located.\
		\
		See the 'Compilation and Linkage' subsection in the README for more\
		information.")
	} else {
		Ok(())
	}
}
