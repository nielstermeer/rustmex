function memlayout(x)
	fprintf('[')
	fprintf('%4i', size(x));
	fprintf('] (n = %i)\n', numel(size(x)));

	for i = 1:numel(x)
		fprintf('[%4i] = %3.0f\n', i, x(i));
	end
end
