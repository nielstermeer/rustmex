# Rustmex Interleaved Complex

This crate is part of the Rustmex family of crates, providing ergonomic bindings to
Matlab's and Octave's MEX APIs from Rust. This crate implements complex numerical arrays
for backends which separate the real and imaginary parts of complex numbers.

You can depend on this crate specifically. Alternatively, you can enable the `complex`
feature flag on the Rustmex crate, which will conspire with other feature flags to pull
in the correct implementation, which are then exposed/`pub use`'d in Rustmex.
