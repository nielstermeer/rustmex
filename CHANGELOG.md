# Rustmex Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [UNRELEASED]

### Added
1. The `type_name` method on `MatlabPtr`, which provides a stable name for the type. It
   is intended to be used in the same vein as `std::any::type_name`, but it yields a
   shorter and stable name. Intended to be used to build `Debug` implementations.
2. The `LhsAns` type, as a transparent replacement for the `Lhs `type. Per the Matlab
   documentation, even if the `mexFunction` gets an `nlhs = 0`, there is still space for
   one return value, which is then placed in the `ans` variable.

## [0.6.3] 2023-06-04

### Fixed
1. The construction of a real numeric array accidentally set the complexity flag. It now
   correctly initialises a real numeric array.

## [0.6.2] 2023-06-04

### Changed
1. The message::Error struct has been changed. Where previously it only held a boxed
   MexMessage, it now in it's From implementation stores the id and message of the
   message, allowing it to be returned from anywhere without worrying about lifetimes.

## [0.6.1] 2023-06-04
No substantive changes but a fix in the generation of the documentation

## [0.6.0] 2023-06-04
Version 6.0 is a major redesign of Rustmex. It is now split up into multiple crates,
namely:
1. `rustmex`, the main crate you'll use;
2. `rustmex_core`, the crate with core rustmex functionality;
3. `rustmex_interleaved_complex`, a crate with the implementations of interleaved complex
   numeric classes;
4. `rustmex_separated_complex`, idem, but for separate complex layout;
5. `rustmex_matlab800`, a crate containing the bindings to the Matlab 800 MEX API;
6. `rustmex_matlab700`, idem, but for a somewhat older matlab which still used the
   separated complex layout for complex arrays; and
7. `rustmex_octave`, containing the bindings for GNU/Octave

This redesign is to make every part of the project compile separately, so that the
feature test matrix does not grow too large.

What has also changed is that all matlab classes (see the changed section) are
represented by their own appropriate wrapper type. Numeric types especially; they are no
longer directly converted to an NDarray. All these wrapper types are called
`MatlabClass`es, which changed its meaning to what it meant previously (something along
the lines of a core matlab primitive, such as an `f64`).

Regardless, this major redesign should make Rustmex as a whole more robust.

### Added
1. Support for Matlab structure, object, and cell arrays.
2. Accepting and transferring responsibility for pointers, instead of just references on
   the MxArray type.
3. Converting empty `mxArray`'s into the corresponding Rust ZST's, and vice versa. Usable
   for composability.

### Changed
1. Selecting no feature for `rustmex` will only cause it to compile, not to bind to a
   backend.
1. `mxArray::class_id` now returns a `Result<ClassID, mxClassID>`, and no longer panics.
   A `ClassID` is a (practically) exhaustive enumeration of the builtin classes. But
   Matlab uses other values of the class id for `classdef` classes. If one tries to get
   the `ClassID` for those, the method errors instead of panicking.

### Fixed
1. Memory leak in the construction of numeric arrays

## [0.5.0] 2022-08-10

### Changed
1. Renamed the `error!` macro in the message module to `trigger_error!` so `error!` can
   be used in the root module.

### Added
1. `error!` macro in the root module, to be used to unconditionally return an Err

## [0.4.2] 10-08-2022

Also yank releases from 0.4.0 (which introduced `NonPersistent<T>`), since they all
contain this bug.

### Fixed
1. `NonPersistent<[u8]>::from_stringish()` (didn't adjust the target slice length to the
   source slice's length, and didn't allocate enough memory for the nul terminator)

## [0.4.1] 09-08-2022 [YANKED]

### Fixed
1. Windows build

## [0.4.0] 08-08-2022 [YANKED]
Another rework of the error handling, hopefully the last one for a while. This fixes
both the leaking of the error object itself (by dropping it before the diverging MEX
error function is called) and the leaking of the message and id strings (copying them to
nonpersistent storage).

### Added
1. The `NonPersistent<T>` type, a smart pointer around a non-persistent allocation of T.

### Removed
1. The `error` function in the library root. Something that might leak memory, with
   alternatives that don't, does not warrant a convenience function.

### Changed
1. `message::error` from a function to a macro.

## [0.3.3] 2022-08-05

### Added
1. `assert` and `error_on` macro, which can be used to assert that some condition is true
   and/or error if some condition is not met. This hides some branches, and should make
   cod which uses this library cleaner.

## [0.3.2] 2022-08-05

### Added
1. Implement `MexMessage` for NDarray's `ShapeError` to make working with NDarray a bit
   more ergonomic.

## [0.3.1] 2022-08-05
### Fixes
1. Dependency on rustmex_entrypoint (was still 0.1)

## [0.3.0] 2022-08-05
Mostly a rework of the error handling, partly to make it more ergonomic, partly to
prevent undue memory leaks.

### Added
1. The `rustmex::Result` type, used with the `Error` object.

### Changed
1. `Error` type, now a wrapper around a `MexMessage` trait object instead of an enum.

## [0.2.0] 2022-08-03

### Added
1. Conversions from `mxArray` to (C)String
2. `Function<_>` smart pointer, for wrapping `mxArrays` which hold `function_handle`s
3. The `Missing` trait, for ergonomically handling missing input variables.
4. Library prelude with important functionality.
5. Functionality to call named Matlab functions (without a function handle).

### Fixed
1. Do not allocate space on `mxArray` initialisation (15a1d91)
2. Check sparsity before converting (15a1d91)

## [0.1.2] 2022-06-28
### Added
1. Owned versions of `mxArray` (`MxArray`), returned when by functions which create
   `mxArray`s. When these are dropped, `DestroyArray` is called, ensuring they're freed
   correctly
2. Conversion from `Vec`-ish types to `mxArrays` (15a1d91)
