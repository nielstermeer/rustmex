# Rustmex

A library providing convenient Rust bindings to Matlab's MEX C api.

Rustmex makes writing MEX functions in Rust a bit easier. It convert Matlab types, and
the arguments it provides to mexFunction into more Rusty types, which can then be used to
interface with other Rust code easily.

## Architecture
The main library is the 'rustmex' crate. This is intended as the user facing crate.

This crate does not directly link to the API of Matlab or Octave. This is because they
each use different symbol names. Instead, Rustmex has a set of crates, which all expose
the same symbol names for the same functionality (the `rustmex_*` functions), which
mostly forward to the appropriate function in the API of the backend.

These different backends also represent complex array differently. Two represent complex
array by storing the real and imaginary part separately; the other stores them
interleaved. What using the aforementioned shim crates also enables is failing
compilation (through a linkage failure) if a numerics crate expecting one representation
(and thus a set of `rustmex_` symbols) is paired with another backend.
