use rustmex::prelude::*;
use rustmex::convert::FromMatlab;
use ndarray::{ArrayViewD, Ix2};

#[rustmex::entrypoint]
fn display_matrix(_lhs: Lhs, rhs: Rhs) -> rustmex::Result<()> {
	if let Some(mx) = rhs.get(0) {
		let mat1 = ArrayViewD::<f64>::from_matlab(mx)?;
		let mat2 = ArrayViewD::<f64>::from_matlab(mx)?
			.into_dimensionality::<Ix2>()?
			.into_shape((1, 4))?;
		dbg!(mat1);
		dbg!(mat2);
	}
	Ok(())
}
