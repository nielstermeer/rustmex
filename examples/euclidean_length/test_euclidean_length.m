clear;

x = rand(1, 10);

l_rs = euclidean_length(x)
l_ml = norm(x)

assert(abs(l_rs - l_ml) < 1e-12);
disp('results match!')
