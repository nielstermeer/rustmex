use rustmex::prelude::*;

// NOTE: these might move into the prelude some day
use rustmex::{
	numeric::{
		// The type used to describe numeric arrays for real numbers
		Numeric,
		// The trait describing numeric arrays in Rustmex. Needed to get at the
		// backing data.
		NumericArray,
	},

	// Trait describing 'Matlab Classes', such as the aforementioned Numeric array.
	MatlabClass,
	convert::{

		// Trait needed to convert 'rust' types (such as f64) back to Matlab.
		ToMatlab,
	},
};

#[rustmex::entrypoint]
fn euclidean_length(lhs: Lhs, rhs: Rhs) -> rustmex::Result<()> {

	// Get the first argument. The Missing trait (in the prelude) adds a function to
	// Options containing mxArrays, allowing for easy error message generation.
	let arg0 = rhs
		.get(0)
		.error_if_missing("euclidean_length:missing_input",
			"Missing input vector to compute the length of")?;

	// With the first argument, an untyped mxArray, you can try converting it into a
	// numeric array. This conversion is of course fallible, returning a result. If
	// the conversion is successful, you can extract the data (via the
	// NumericArray::data method)
	let v = Numeric::<f64,_>::from_mx_array(arg0)?.data();

	// I recommend to only execute your planned computation once you're sure it
	// actually needs to be computed — it's a bit of a waste to compute an expensive
	// result without somewhere to return it to.
	if let Some(ret) = lhs.get_mut(0) {
		// Do the computation. In this case we compute the magnitude of an
		// N-dimensional vector.
		let len = v.iter().map(|x|x*x).sum::<f64>().sqrt();

		// The result of that computation is an f64, in this case. We can't just
		// return a double to Matlab; it needs to be converted to it's types.
		// (Under the hood it boxes the double and stores it in an owned mxArray,
		// an MxArray)
		let owned_mx = len.to_matlab();

		// With the value converted, it can be placed in the lhs array. When your
		// MEX function returns to Matlab, the result will appear in the lhs of
		// the call in Matlab.
		ret.replace(owned_mx);
	}

	// Indicate the no-error state.
	Ok(())
}
