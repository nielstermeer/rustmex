use rustmex::prelude::*;

// Since ~v0.6, the entrypoint has been set up to catch unwinding panics. If your Rust
// MEX file has been compiled with `panic="unwind"`, instead of aborting the whole
// program, the panic is caught at the interface and returned to the Matlab prompt.
#[rustmex::entrypoint]
fn catch_panic(_lhs: Lhs, _rhs: Rhs) -> rustmex::Result<()> {
	call_from_main();
	Ok(())
}

fn call_from_main() {
	call_from_child();
}

// You even get the whole call chain.
fn call_from_child() {
	panic!("This is a test");
}
