use rustmex::prelude::*;

// NOTE: these might move into the prelude some day
use rustmex::{
	numeric::{
		Numeric,
		NumericArray,
	},
	MatlabClass,
	convert::{
		ToMatlab,
	},
};

#[rustmex::entrypoint]
fn euclidean_length(mut lhs: LhsAns, rhs: Rhs) -> rustmex::Result<()> {
	/*
	 * This is basically the euclidean_length example, but instead of checking
	 * whether nargout >= 1, we compute and return the euclidean length
	 * unconditionally, writing it to the explitit variable or have Matlab write it
	 * to ans.
	 */

	let arg0 = rhs
		.get(0)
		.error_if_missing("euclidean_length:missing_input",
			"Missing input vector to compute the length of")?;

	let v = Numeric::<f64,_>::from_mx_array(arg0)?.data();
	let len = v.iter().map(|x|x*x).sum::<f64>().sqrt();
	let owned_mx = len.to_matlab();

	lhs.ans_mut().replace(owned_mx);

	Ok(())
}
