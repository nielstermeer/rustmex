# Rustmex Examples

This workspace contains a set of working and tested examples of how Rustmex can be used.
While sparse, it is hopefully a good introduction to get you started with Rustmex. More
examples may follow at a later date. If any example is unclear, or you feel like there's
something missing, please file an issue. These examples also serve as tests, so if they
don't work, please also file an issue.

## Suggested Reading Order
1. `hello_world`
2. `catch_panic/`
3. `euclidean_length/`
4. `ndarray_demo/`
5. `display_matrix/`
6. `function_calls/`

## Trying these examples yourself
When you `cargo build --all` on Linux, the symlinks are set up such that you can
immediately try out the examples in each of their directories, either via the `test_*.m`
scripts in each directory, or by calling the functions directly. On Mac or Windows, you
might need to do some relinking/copying of the compiled libraries with the appropriate
extensions.
