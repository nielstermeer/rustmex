use rustmex::prelude::*;
use rustmex::MatlabClass;

// The MatlabClass used to represent function handles
use rustmex::function::Function;

#[rustmex::entrypoint]
fn rusty_fn_call(lhs: Lhs, rhs: Rhs) -> rustmex::Result<()> {
	let f = Function::from_mx_array(rhs
		.get(0)
		.error_if_missing("rusthello:no_fn", "Didn't get a function")?)?;

	if let Some(r) = lhs.get_mut(0) {
		// Just forward the remaining arguments
		let mut results = f.call(1, &rhs[1..]).unwrap();

		let v = results[0].take().unwrap();

		r.replace(v);
	};

	return Ok(());
}
