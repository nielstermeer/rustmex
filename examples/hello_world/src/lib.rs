use rustmex::prelude::*;

// A very basic example of a Rustmex MEX function

// The entrypoint of your program is annotated with the `entrypoint` proc-macro. It
// generates the actual entrypoint for you, taking care of the gluing details. The name
// of the function you attach it to does not matter, as long as there is only one
// invocation of the `entrypoint` macro.
//
// The function itself does have to have a particular signature. It has to accept the
// "left hand side" and "right hand side" as arguments, and return a rustmex::Result.
// The LHS and correspond to the following on the Matlab side
// [lhs0, lhs1, ..., lhsn] = f(rhs0, rhs1, ..., rhsn);
//
// The Result is how you can indicate meaningful errors. Errors which Rustmex generates,
// can be converted into this Result type.
#[rustmex::entrypoint]
fn hello_world(_lhs: Lhs, _rhs: Rhs) -> rustmex::Result<()> {

	// Notably, the usual 'rust' tooling also works within mex files.
	println!("Hello Matlab!");
	Ok(())
}
