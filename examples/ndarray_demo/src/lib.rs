use rustmex::prelude::*;

// Rustmex comes with built-in support for NDarray, so it can faithfully convert Matlab
// matrices to rust NDarrays.
use ndarray::ArrayViewD;

// Since an ArrayViewD isn't a 'MatlabClass', there is a different trait to allow direct
// conversions from mxArrays to 'other' types. Under the hood this invokes the
// MatlabClass::from_mx_array method, but it also takes care that the shape of the array
// is the same as in matlab.
use rustmex::convert::FromMatlab;

#[rustmex::entrypoint]
fn display_matrix(_lhs: Lhs, rhs: Rhs) -> rustmex::Result<()> {
	if let Some(mx) = rhs.get(0) {
		// As per usual, there is no type inference in the conversion step, you
		// need to indicate explicitly what type you want.
		let mat = ArrayViewD::<f64>::from_matlab(mx)?;
		eprintln!("{mat:#?}");
	}
	Ok(())
}
