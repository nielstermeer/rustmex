# Rustmex Matlab700 backend

This crate is part of the Rustmex family of crates, providing convenient bindings to
Matlab's and Octave's MEX APIs. This crate exposes well-known symbols for within Rustmex,
binding to the Matlab700 symbol names.

You should not need to depend on this crate directly. Instead consider enabling the
`matlab700` feature flag on Rustmex, which selects this crate as the backend. If
selected, you can use Rustmex with older versions of Matlab.
