#![allow(non_camel_case_types)]
use crate::renamed::*;

use ::std::os::raw::{
	c_void,
	c_int,
	c_char,
};

use rustmex_core::raw::*;

use rustmex_core::mappable::Real;
use num_complex::Complex;

type mxClassID = u32;
type mxComplexity = u32;

// alloc
#[no_mangle]
pub unsafe fn rustmex_malloc(n: usize) -> *mut c_void {
	mxMalloc(n as _)
}

#[no_mangle]
pub unsafe fn rustmex_free(ptr: *mut c_void) {
	mxFree(ptr)
}

#[no_mangle]
pub unsafe fn rustmex_make_memory_persistent(ptr: *mut c_void) {
	mexMakeMemoryPersistent(ptr)
}

// general mxArray
#[no_mangle]
pub unsafe fn rustmex_get_dimensions(mx: *const mxArray) -> *const usize {
	mxGetDimensions(mx) as _
}

#[no_mangle]
pub unsafe fn rustmex_get_number_of_dimensions(mx: *const mxArray) -> usize {
	mxGetNumberOfDimensions(mx).try_into().expect("Number of dimensions should be positive integer")
}

#[no_mangle]
pub unsafe fn rustmex_numel(mx: *const mxArray) -> usize {
	mxGetNumberOfElements(mx).try_into().expect("Number of elements should be positive integer")
}

#[no_mangle]
pub unsafe fn rustmex_get_class_id(mx: *const mxArray) -> mxClassID {
	mxGetClassID(mx)
}

#[no_mangle]
pub unsafe fn rustmex_is_complex(mx: *const mxArray) -> bool {
	mxIsComplex(mx)
}

#[no_mangle]
pub unsafe fn rustmex_is_sparse(mx: *const mxArray) -> bool {
	mxIsSparse(mx)
}

#[no_mangle]
pub unsafe fn rustmex_is_struct(mx: *const mxArray) -> bool {
	mxIsStruct(mx)
}

#[no_mangle]
pub unsafe fn rustmex_is_class(mx: *const mxArray, name: *const c_char) -> bool {
	mxIsClass(mx, name)
}

#[no_mangle]
pub unsafe fn rustmex_duplicate_array(mx: *const mxArray) -> *mut mxArray {
	mxDuplicateArray(mx)
}

#[no_mangle]
pub unsafe fn rustmex_destroy_array(mx: *mut mxArray) {
	mxDestroyArray(mx)
}

#[no_mangle]
pub unsafe fn rustmex_create_uninit_numeric_array(
        ndim: usize,
        dims: *const usize,
        classid: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray
{
	mxCreateUninitNumericArray(ndim, dims as _, classid, flag)
}

#[no_mangle]
pub unsafe fn rustmex_set_dimensions(mx: *mut mxArray, dims: *const usize, ndims: usize) -> i32 {
	mxSetDimensions(mx, dims, ndims)
}

#[no_mangle]
pub unsafe fn rustmex_get_data(mx: *const mxArray) -> *mut c_void {
	mxGetData(mx)
}

#[no_mangle]
pub unsafe fn rustmex_get_imag_data(mx: *const mxArray) -> *mut c_void {
	mxGetImagData(mx)
}

#[no_mangle]
pub unsafe fn rustmex_get_separated_complex(mx: *const mxArray) -> Complex<*mut c_void> {
	Complex {
		re: mxGetData(mx),
		im: mxGetImagData(mx)
	}
}

#[no_mangle]
pub unsafe fn rustmex_set_data(mx: *mut mxArray, newdata: *mut c_void) {
	mxSetData(mx, newdata)
}

#[no_mangle]
pub unsafe fn rustmex_set_imag_data(mx: *mut mxArray, newdata: *mut c_void) {
	mxSetImagData(mx, newdata)
}

#[no_mangle]
pub unsafe fn rustmex_set_separated_complex(mx: *mut mxArray, newdata: Complex<*mut c_void>) {
	mxSetData(mx, newdata.re);
	mxSetImagData(mx, newdata.im);
}

// structs
#[no_mangle]
pub unsafe fn rustmex_get_number_of_fields(mx: *const mxArray) -> c_int {
	mxGetNumberOfFields(mx)
}

#[no_mangle]
pub unsafe fn rustmex_get_field_number(mx: *const mxArray, name: *const c_char) -> c_int {
	mxGetFieldNumber(mx, name)
}

#[no_mangle]
pub unsafe fn rustmex_get_field_name_by_number(mx: *const mxArray, num: c_int) -> *const c_char {
	mxGetFieldNameByNumber(mx, num)
}

#[no_mangle]
pub unsafe fn rustmex_get_field_by_number(mx: *const mxArray, linidx: mwIndex, fieldnum: c_int) -> *mut mxArray {
	mxGetFieldByNumber(mx, linidx, fieldnum)
}

#[no_mangle]
pub unsafe fn rustmex_set_field_by_number(mx: *mut mxArray, linidx: mwIndex, fieldnum: c_int, data: *mut mxArray) {
	mxSetFieldByNumber(mx, linidx, fieldnum, data)
}

#[no_mangle]
pub unsafe fn rustmex_add_field(mx: *mut mxArray, fieldname: *const c_char) -> c_int {
	mxAddField(mx, fieldname)
}

#[no_mangle]
pub unsafe fn rustmex_remove_field(mx: *mut mxArray, fieldnum: c_int) {
	mxRemoveField(mx, fieldnum)
}

#[no_mangle]
pub unsafe fn rustmex_create_struct_array(ndims: mwSize, dims: *const mwSize, nfields: c_int, fieldnames: *mut *const c_char) -> *mut mxArray {
	mxCreateStructArray(ndims, dims, nfields, fieldnames)
}

// objects
#[no_mangle]
pub unsafe fn rustmex_get_property(mx: *const mxArray, linidx: mwIndex, propname: *const c_char) -> *mut mxArray {
	mxGetProperty(mx, linidx, propname)
}

#[no_mangle]
pub unsafe fn rustmex_set_property(mx: *mut mxArray, linidx: mwIndex, propname: *const c_char, value: *const mxArray) {
	mxSetProperty(mx, linidx, propname, value)
}

// cell
#[no_mangle]
pub unsafe fn rustmex_get_cell(mx: *const mxArray, i: mwIndex) -> *mut mxArray {
	mxGetCell(mx, i)
}

#[no_mangle]
pub unsafe fn rustmex_set_cell(mx: *mut mxArray, i: mwIndex, val: *mut mxArray) {
	mxSetCell(mx, i, val)
}

#[no_mangle]
pub unsafe fn rustmex_create_cell_array(ndims: mwSize, dims: *const mwSize) -> *mut mxArray {
	mxCreateCellArray(ndims, dims)
}

// workspace
#[no_mangle]
pub unsafe fn rustmex_get_variable_ptr(workspace: *const c_char, name: *const c_char) -> *const mxArray {
	mexGetVariablePtr(workspace, name)
}

#[no_mangle]
pub unsafe fn rustmex_put_variable(workspace: *const c_char, name: *const c_char, val: *mut mxArray) -> c_int {
	mexPutVariable(workspace, name, val)
}

// function calls
#[no_mangle]
pub unsafe fn rustmex_call_matlab(nlhs: c_int, lhs: *mut *mut mxArray, nrhs: c_int, rhs: *mut *mut mxArray, fname: *const c_char) -> c_int {
	mexCallMATLAB(nlhs, lhs, nrhs, rhs, fname)
}

#[no_mangle]
pub unsafe fn rustmex_call_matlab_with_trap(nlhs: c_int, lhs: *mut *mut mxArray, nrhs: c_int, rhs: *mut *mut mxArray, fname: *const c_char) -> *mut mxArray {
	mexCallMATLABWithTrap(nlhs, lhs, nrhs, rhs, fname)
}

// messages
#[no_mangle]
pub unsafe fn rustmex_warn_id_and_txt(id: *const c_char, msg: *const c_char) {
	mexWarnMsgIdAndTxt(id, msg)
}

#[no_mangle]
pub unsafe fn rustmex_err_id_and_txt(id: *const c_char, msg: *const c_char) {
	mexErrMsgIdAndTxt(id, msg)
}

// extra type safety wrappers

#[no_mangle]
pub unsafe fn rustmex_set_real_data(mx: *mut mxArray, newdata: Real<*mut c_void>) {
	mxSetData(mx, newdata.0)
}

#[no_mangle]
pub unsafe fn rustmex_get_real_data(mx: *const mxArray) -> Real<*mut c_void> {
	Real(mxGetData(mx))
}

// Stringy functions
#[no_mangle]
pub unsafe fn rustmex_array_to_cstring(mx: *const mxArray) -> *mut c_char {
	mxArrayToString(mx)
}
