// This file contains the bindings matlab exposes, so we shouldn't care about their style
#![allow(bad_style)]
#![allow(unused)]

use rustmex_core::raw::*;
pub type SLIndex = i64;
pub type SLSize = i64;
pub type CHAR16_T = ::std::os::raw::c_ushort;
type size_t = usize;

#[doc = " MEX-file entry point type"]
pub type mxFunctionPtr = ::std::option::Option<
    unsafe extern "C" fn(
        nlhs: ::std::os::raw::c_int,
        plhs: *mut *mut mxArray,
        nrhs: ::std::os::raw::c_int,
        prhs: *mut *mut mxArray,
    ),
>;
#[doc = " Logical type"]
pub type mxLogical = bool;
#[doc = " Required for Unicode support in MATLAB"]
pub type mxChar = CHAR16_T;
#[doc = " mxArray classes."]
pub type mxClassID = ::std::os::raw::c_uint;
#[doc = " Indicates whether floating-point mxArrays are real or complex."]
pub type mxComplexity = ::std::os::raw::c_uint;
pub type mxDouble = f64;
pub type mxSingle = f32;
pub type mxInt8 = i8;
pub type mxUint8 = u8;
pub type mxInt16 = i16;
pub type mxUint16 = u16;
pub type mxInt32 = i32;
pub type mxUint32 = u32;
pub type mxInt64 = i64;
pub type mxUint64 = u64;

extern "C" {
    pub fn mxMalloc(n: size_t) -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxCalloc(n: size_t, size: size_t) -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxFree(ptr: *mut ::std::os::raw::c_void);
}
extern "C" {
    pub fn mxRealloc(ptr: *mut ::std::os::raw::c_void, size: size_t)
        -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxGetNumberOfDimensions_730(pa: *const mxArray) -> mwSize;
}
extern "C" {
    pub fn mxGetDimensions_730(pa: *const mxArray) -> *const mwSize;
}
extern "C" {
    pub fn mxGetM(pa: *const mxArray) -> size_t;
}
extern "C" {
    pub fn mxGetIr_730(pa: *const mxArray) -> *mut mwIndex;
}
extern "C" {
    pub fn mxGetJc_730(pa: *const mxArray) -> *mut mwIndex;
}
extern "C" {
    pub fn mxGetNzmax_730(pa: *const mxArray) -> mwSize;
}
extern "C" {
    pub fn mxSetNzmax_730(pa: *mut mxArray, nzmax: mwSize);
}
extern "C" {
    pub fn mxGetFieldNameByNumber(
        pa: *const mxArray,
        n: ::std::os::raw::c_int,
    ) -> *const ::std::os::raw::c_char;
}
extern "C" {
    pub fn mxGetFieldByNumber_730(
        pa: *const mxArray,
        i: mwIndex,
        fieldnum: ::std::os::raw::c_int,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxGetCell_730(pa: *const mxArray, i: mwIndex) -> *mut mxArray;
}
extern "C" {
    pub fn mxGetClassID(pa: *const mxArray) -> mxClassID;
}
extern "C" {
    pub fn mxGetData(pa: *const mxArray) -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxSetData(pa: *mut mxArray, newdata: *mut ::std::os::raw::c_void);
}
extern "C" {
    pub fn mxIsNumeric(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsCell(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsLogical(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsScalar(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsChar(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsStruct(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsOpaque(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsFunctionHandle(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsObject(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxGetImagData(pa: *const mxArray) -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxSetImagData(pa: *mut mxArray, newdata: *mut ::std::os::raw::c_void);
}
extern "C" {
    pub fn mxIsComplex(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsSparse(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsDouble(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsSingle(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsInt8(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsUint8(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsInt16(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsUint16(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsInt32(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsUint32(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsInt64(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsUint64(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxGetNumberOfElements(pa: *const mxArray) -> size_t;
}
extern "C" {
    pub fn mxGetPi(pa: *const mxArray) -> *mut f64;
}
extern "C" {
    pub fn mxSetPi(pa: *mut mxArray, pi: *mut f64);
}
extern "C" {
    pub fn mxGetChars(pa: *const mxArray) -> *mut mxChar;
}
extern "C" {
    pub fn mxGetUserBits(pa: *const mxArray) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxSetUserBits(pa: *mut mxArray, value: ::std::os::raw::c_int);
}
extern "C" {
    pub fn mxGetScalar(pa: *const mxArray) -> f64;
}
extern "C" {
    pub fn mxIsFromGlobalWS(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxSetFromGlobalWS(pa: *mut mxArray, global: bool);
}
extern "C" {
    pub fn mxSetM_730(pa: *mut mxArray, m: mwSize);
}
extern "C" {
    pub fn mxGetN(pa: *const mxArray) -> size_t;
}
extern "C" {
    pub fn mxIsEmpty(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxGetFieldNumber(
        pa: *const mxArray,
        name: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxSetIr_730(pa: *mut mxArray, newir: *mut mwIndex);
}
extern "C" {
    pub fn mxSetJc_730(pa: *mut mxArray, newjc: *mut mwIndex);
}
extern "C" {
    pub fn mxGetPr(pa: *const mxArray) -> *mut f64;
}
extern "C" {
    pub fn mxSetPr(pa: *mut mxArray, newdata: *mut f64);
}
extern "C" {
    pub fn mxGetElementSize(pa: *const mxArray) -> size_t;
}
extern "C" {
    pub fn mxCalcSingleSubscript_730(
        pa: *const mxArray,
        nsubs: mwSize,
        subs: *const mwIndex,
    ) -> mwIndex;
}
extern "C" {
    pub fn mxGetNumberOfFields(pa: *const mxArray) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxSetCell_730(pa: *mut mxArray, i: mwIndex, value: *mut mxArray);
}
extern "C" {
    pub fn mxSetFieldByNumber_730(
        pa: *mut mxArray,
        i: mwIndex,
        fieldnum: ::std::os::raw::c_int,
        value: *mut mxArray,
    );
}
extern "C" {
    pub fn mxGetField_730(
        pa: *const mxArray,
        i: mwIndex,
        fieldname: *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxSetField_730(
        pa: *mut mxArray,
        i: mwIndex,
        fieldname: *const ::std::os::raw::c_char,
        value: *mut mxArray,
    );
}
extern "C" {
    pub fn mxGetProperty_730(
        pa: *const mxArray,
        i: mwIndex,
        propname: *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxSetProperty_730(
        pa: *mut mxArray,
        i: mwIndex,
        propname: *const ::std::os::raw::c_char,
        value: *const mxArray,
    );
}
extern "C" {
    pub fn mxGetClassName(pa: *const mxArray) -> *const ::std::os::raw::c_char;
}
extern "C" {
    pub fn mxIsClass(pa: *const mxArray, name: *const ::std::os::raw::c_char) -> bool;
}
extern "C" {
    pub fn mxCreateNumericMatrix_730(
        m: mwSize,
        n: mwSize,
        classid: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateUninitNumericMatrix(
        m: size_t,
        n: size_t,
        classid: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateUninitNumericArray(
        ndim: size_t,
        dims: *mut size_t,
        classid: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxSetN_730(pa: *mut mxArray, n: mwSize);
}
extern "C" {
    pub fn mxSetDimensions_730(
        pa: *mut mxArray,
        pdims: *const mwSize,
        ndims: mwSize,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxDestroyArray(pa: *mut mxArray);
}
extern "C" {
    pub fn mxCreateNumericArray_730(
        ndim: mwSize,
        dims: *const mwSize,
        classid: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateCharArray_730(ndim: mwSize, dims: *const mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateDoubleMatrix_730(m: mwSize, n: mwSize, flag: mxComplexity) -> *mut mxArray;
}
extern "C" {
    pub fn mxGetLogicals(pa: *const mxArray) -> *mut mxLogical;
}
extern "C" {
    pub fn mxCreateLogicalArray_730(ndim: mwSize, dims: *const mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateLogicalMatrix_730(m: mwSize, n: mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateLogicalScalar(value: bool) -> *mut mxArray;
}
extern "C" {
    pub fn mxIsLogicalScalar(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsLogicalScalarTrue(pa: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxCreateDoubleScalar(value: f64) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateSparse_730(
        m: mwSize,
        n: mwSize,
        nzmax: mwSize,
        flag: mxComplexity,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateSparseLogicalMatrix_730(m: mwSize, n: mwSize, nzmax: mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxGetNChars_730(pa: *const mxArray, buf: *mut ::std::os::raw::c_char, nChars: mwSize);
}
extern "C" {
    pub fn mxGetString_730(
        pa: *const mxArray,
        buf: *mut ::std::os::raw::c_char,
        buflen: mwSize,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxArrayToString(pa: *const mxArray) -> *mut ::std::os::raw::c_char;
}
extern "C" {
    pub fn mxArrayToUTF8String(pa: *const mxArray) -> *mut ::std::os::raw::c_char;
}
extern "C" {
    pub fn mxCreateStringFromNChars_730(
        str_: *const ::std::os::raw::c_char,
        n: mwSize,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateString(str_: *const ::std::os::raw::c_char) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateCharMatrixFromStrings_730(
        m: mwSize,
        str_: *mut *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateCellMatrix_730(m: mwSize, n: mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateCellArray_730(ndim: mwSize, dims: *const mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateStructMatrix_730(
        m: mwSize,
        n: mwSize,
        nfields: ::std::os::raw::c_int,
        fieldnames: *mut *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateStructArray_730(
        ndim: mwSize,
        dims: *const mwSize,
        nfields: ::std::os::raw::c_int,
        fieldnames: *mut *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxDuplicateArray(in_: *const mxArray) -> *mut mxArray;
}
extern "C" {
    pub fn mxSetClassName(
        pa: *mut mxArray,
        classname: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxAddField(
        pa: *mut mxArray,
        fieldname: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxRemoveField(pa: *mut mxArray, field: ::std::os::raw::c_int);
}
extern "C" {
    pub fn mxGetEps() -> f64;
}
extern "C" {
    pub fn mxGetInf() -> f64;
}
extern "C" {
    pub fn mxGetNaN() -> f64;
}
extern "C" {
    pub fn mxIsFinite(x: f64) -> bool;
}
extern "C" {
    pub fn mxIsInf(x: f64) -> bool;
}
extern "C" {
    pub fn mxIsNaN(x: f64) -> bool;
}
extern "C" {
    pub fn mxCreateSharedDataCopy(pa: *const mxArray) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateUninitDoubleMatrix(
        cmplx_flag: ::std::os::raw::c_int,
        m: size_t,
        n: size_t,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxFastZeros(
        cmplx_flag: ::std::os::raw::c_int,
        m: ::std::os::raw::c_int,
        n: ::std::os::raw::c_int,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxUnreference(pa: *mut mxArray) -> *mut mxArray;
}
extern "C" {
    pub fn mxUnshareArray(pa: *mut mxArray, level: ::std::os::raw::c_int) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxGetPropertyShared(
        pa: *const mxArray,
        i: size_t,
        propname: *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxSetPropertyShared(
        pa: *mut mxArray,
        i: size_t,
        propname: *const ::std::os::raw::c_char,
        value: *const mxArray,
    );
}
extern "C" {
    pub fn __assert_fail(
        __assertion: *const ::std::os::raw::c_char,
        __file: *const ::std::os::raw::c_char,
        __line: ::std::os::raw::c_uint,
        __function: *const ::std::os::raw::c_char,
    );
}
extern "C" {
    pub fn __assert_perror_fail(
        __errnum: ::std::os::raw::c_int,
        __file: *const ::std::os::raw::c_char,
        __line: ::std::os::raw::c_uint,
        __function: *const ::std::os::raw::c_char,
    );
}
extern "C" {
    pub fn __assert(
        __assertion: *const ::std::os::raw::c_char,
        __file: *const ::std::os::raw::c_char,
        __line: ::std::os::raw::c_int,
    );
}
pub type mex_exit_fn = ::std::option::Option<unsafe extern "C" fn()>;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct mexGlobalTableEntry_Tag {
    pub name: *const ::std::os::raw::c_char,
    pub variable: *mut *mut mxArray,
}
#[test]
fn bindgen_test_layout_mexGlobalTableEntry_Tag() {
    assert_eq!(
        ::std::mem::size_of::<mexGlobalTableEntry_Tag>(),
        16usize,
        concat!("Size of: ", stringify!(mexGlobalTableEntry_Tag))
    );
    assert_eq!(
        ::std::mem::align_of::<mexGlobalTableEntry_Tag>(),
        8usize,
        concat!("Alignment of ", stringify!(mexGlobalTableEntry_Tag))
    );
    fn test_field_name() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<mexGlobalTableEntry_Tag>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).name) as usize - ptr as usize
            },
            0usize,
            concat!(
                "Offset of field: ",
                stringify!(mexGlobalTableEntry_Tag),
                "::",
                stringify!(name)
            )
        );
    }
    test_field_name();
    fn test_field_variable() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<mexGlobalTableEntry_Tag>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).variable) as usize - ptr as usize
            },
            8usize,
            concat!(
                "Offset of field: ",
                stringify!(mexGlobalTableEntry_Tag),
                "::",
                stringify!(variable)
            )
        );
    }
    test_field_variable();
}
pub type mexGlobalTableEntry = mexGlobalTableEntry_Tag;
pub type mexGlobalTable = *mut mexGlobalTableEntry_Tag;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct mexFunctionTableEntry_tag {
    pub name: *const ::std::os::raw::c_char,
    pub f: mxFunctionPtr,
    pub nargin: ::std::os::raw::c_int,
    pub nargout: ::std::os::raw::c_int,
    pub local_function_table: *mut _mexLocalFunctionTable,
}
#[test]
fn bindgen_test_layout_mexFunctionTableEntry_tag() {
    assert_eq!(
        ::std::mem::size_of::<mexFunctionTableEntry_tag>(),
        32usize,
        concat!("Size of: ", stringify!(mexFunctionTableEntry_tag))
    );
    assert_eq!(
        ::std::mem::align_of::<mexFunctionTableEntry_tag>(),
        8usize,
        concat!("Alignment of ", stringify!(mexFunctionTableEntry_tag))
    );
    fn test_field_name() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<mexFunctionTableEntry_tag>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).name) as usize - ptr as usize
            },
            0usize,
            concat!(
                "Offset of field: ",
                stringify!(mexFunctionTableEntry_tag),
                "::",
                stringify!(name)
            )
        );
    }
    test_field_name();
    fn test_field_f() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<mexFunctionTableEntry_tag>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).f) as usize - ptr as usize
            },
            8usize,
            concat!(
                "Offset of field: ",
                stringify!(mexFunctionTableEntry_tag),
                "::",
                stringify!(f)
            )
        );
    }
    test_field_f();
    fn test_field_nargin() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<mexFunctionTableEntry_tag>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).nargin) as usize - ptr as usize
            },
            16usize,
            concat!(
                "Offset of field: ",
                stringify!(mexFunctionTableEntry_tag),
                "::",
                stringify!(nargin)
            )
        );
    }
    test_field_nargin();
    fn test_field_nargout() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<mexFunctionTableEntry_tag>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).nargout) as usize - ptr as usize
            },
            20usize,
            concat!(
                "Offset of field: ",
                stringify!(mexFunctionTableEntry_tag),
                "::",
                stringify!(nargout)
            )
        );
    }
    test_field_nargout();
    fn test_field_local_function_table() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<mexFunctionTableEntry_tag>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).local_function_table) as usize - ptr as usize
            },
            24usize,
            concat!(
                "Offset of field: ",
                stringify!(mexFunctionTableEntry_tag),
                "::",
                stringify!(local_function_table)
            )
        );
    }
    test_field_local_function_table();
}
pub type mexFunctionTableEntry = mexFunctionTableEntry_tag;
pub type mexFunctionTable = *mut mexFunctionTableEntry_tag;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _mexLocalFunctionTable {
    pub length: size_t,
    pub entries: mexFunctionTable,
}
#[test]
fn bindgen_test_layout__mexLocalFunctionTable() {
    assert_eq!(
        ::std::mem::size_of::<_mexLocalFunctionTable>(),
        16usize,
        concat!("Size of: ", stringify!(_mexLocalFunctionTable))
    );
    assert_eq!(
        ::std::mem::align_of::<_mexLocalFunctionTable>(),
        8usize,
        concat!("Alignment of ", stringify!(_mexLocalFunctionTable))
    );
    fn test_field_length() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<_mexLocalFunctionTable>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).length) as usize - ptr as usize
            },
            0usize,
            concat!(
                "Offset of field: ",
                stringify!(_mexLocalFunctionTable),
                "::",
                stringify!(length)
            )
        );
    }
    test_field_length();
    fn test_field_entries() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<_mexLocalFunctionTable>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).entries) as usize - ptr as usize
            },
            8usize,
            concat!(
                "Offset of field: ",
                stringify!(_mexLocalFunctionTable),
                "::",
                stringify!(entries)
            )
        );
    }
    test_field_entries();
}
pub type mexLocalFunctionTable = *mut _mexLocalFunctionTable;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _mexInitTermTableEntry {
    pub initialize: ::std::option::Option<unsafe extern "C" fn()>,
    pub terminate: ::std::option::Option<unsafe extern "C" fn()>,
}
#[test]
fn bindgen_test_layout__mexInitTermTableEntry() {
    assert_eq!(
        ::std::mem::size_of::<_mexInitTermTableEntry>(),
        16usize,
        concat!("Size of: ", stringify!(_mexInitTermTableEntry))
    );
    assert_eq!(
        ::std::mem::align_of::<_mexInitTermTableEntry>(),
        8usize,
        concat!("Alignment of ", stringify!(_mexInitTermTableEntry))
    );
    fn test_field_initialize() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<_mexInitTermTableEntry>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).initialize) as usize - ptr as usize
            },
            0usize,
            concat!(
                "Offset of field: ",
                stringify!(_mexInitTermTableEntry),
                "::",
                stringify!(initialize)
            )
        );
    }
    test_field_initialize();
    fn test_field_terminate() {
        assert_eq!(
            unsafe {
                let uninit = ::std::mem::MaybeUninit::<_mexInitTermTableEntry>::uninit();
                let ptr = uninit.as_ptr();
                ::std::ptr::addr_of!((*ptr).terminate) as usize - ptr as usize
            },
            8usize,
            concat!(
                "Offset of field: ",
                stringify!(_mexInitTermTableEntry),
                "::",
                stringify!(terminate)
            )
        );
    }
    test_field_terminate();
}
pub type mexInitTermTableEntry = *mut _mexInitTermTableEntry;
pub type fn_clean_up_after_error = ::std::option::Option<unsafe extern "C" fn()>;
pub type fn_simple_function_to_string =
    ::std::option::Option<unsafe extern "C" fn(f: mxFunctionPtr) -> *const ::std::os::raw::c_char>;
pub type fn_mex_get_local_function_table =
    ::std::option::Option<unsafe extern "C" fn() -> mexLocalFunctionTable>;
pub type fn_mex_set_local_function_table = ::std::option::Option<
    unsafe extern "C" fn(arg1: mexLocalFunctionTable) -> mexLocalFunctionTable,
>;
extern "C" {
    pub fn mexErrMsgTxt(error_msg: *const ::std::os::raw::c_char);
}
extern "C" {
    pub fn mexErrMsgIdAndTxt(
        identifier: *const ::std::os::raw::c_char,
        err_msg: *const ::std::os::raw::c_char,
        ...
    );
}
extern "C" {
    pub fn mexWarnMsgTxt(warn_msg: *const ::std::os::raw::c_char);
}
extern "C" {
    pub fn mexWarnMsgIdAndTxt(
        identifier: *const ::std::os::raw::c_char,
        warn_msg: *const ::std::os::raw::c_char,
        ...
    );
}
extern "C" {
    pub fn mexPrintf(fmt: *const ::std::os::raw::c_char, ...) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexMakeArrayPersistent(pa: *mut mxArray);
}
extern "C" {
    pub fn mexMakeMemoryPersistent(ptr: *mut ::std::os::raw::c_void);
}
extern "C" {
    pub fn mexCallMATLABWithObject(
        nlhs: ::std::os::raw::c_int,
        plhs: *mut *mut mxArray,
        nrhs: ::std::os::raw::c_int,
        prhs: *mut *mut mxArray,
        fcn_name: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexCallMATLABWithTrapWithObject(
        nlhs: ::std::os::raw::c_int,
        plhs: *mut *mut mxArray,
        nrhs: ::std::os::raw::c_int,
        prhs: *mut *mut mxArray,
        fcn_name: *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mexPrintAssertion(
        test: *const ::std::os::raw::c_char,
        fname: *const ::std::os::raw::c_char,
        linenum: ::std::os::raw::c_int,
        message: *const ::std::os::raw::c_char,
    );
}
extern "C" {
    pub fn mexIsGlobal(pA: *const mxArray) -> bool;
}
extern "C" {
    pub fn mexPutVariable(
        workspace: *const ::std::os::raw::c_char,
        name: *const ::std::os::raw::c_char,
        parray: *const mxArray,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexGetVariablePtr(
        workspace: *const ::std::os::raw::c_char,
        name: *const ::std::os::raw::c_char,
    ) -> *const mxArray;
}
extern "C" {
    pub fn mexGetVariableWithObject(
        workspace: *const ::std::os::raw::c_char,
        name: *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mexLock();
}
extern "C" {
    pub fn mexUnlock();
}
extern "C" {
    pub fn mexIsLocked() -> bool;
}
extern "C" {
    pub fn mexFunctionName() -> *const ::std::os::raw::c_char;
}
extern "C" {
    pub fn mexEvalString(str_: *const ::std::os::raw::c_char) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexEvalStringWithTrap(str_: *const ::std::os::raw::c_char) -> *mut mxArray;
}
extern "C" {
    pub fn mexAtExit(exit_fcn: mex_exit_fn) -> ::std::os::raw::c_int;
}
