#include <stdio.h>
#include <stdint.h>

#include "mex.h"

/*
 * Compile with:
 * $(CC) gcc -I $(MATLABROOT)/extern/include/ -shared -fpic memlayout.c -o memlayout.mexa64
 */

static uint64_t numel(const uint64_t *dims, uint64_t numdims) {
	if (numdims == 0) {
		return 0;
	}
	uint64_t num = dims[0];

	for (int i = 1; i < numdims; i++) {
		num *= dims[i];
	}

	return num;
}

void mexFunction(int nlhs, mxArray **lhs, int nrhs, const mxArray **rhs) {
	if (nrhs < 1) {
		return;
	}

	uint64_t numdims = mxGetNumberOfDimensions_800(rhs[0]);
	const uint64_t *dims = mxGetDimensions_800(rhs[0]);
	double *data = mxGetDoubles_800(rhs[0]);

	if (!data) {
		printf("Expected doubles, got something else");
		return;
	}

	printf("[");
	for (int i = 0; i < numdims; i++) {
		printf("%4i", dims[i]);
	}
	printf("] (n = %i)\n", numdims);
	uint64_t num = numel(dims, numdims);
	for (int i = 0; i < num; i++) {
		printf("[%4i] = %3.0f\n", i, data[i]);
	}
}

