// This file contains the bindings matlab exposes, so we shouldn't care about their style
#![allow(bad_style)]
#![allow(unused)]

use rustmex_core::raw::*;
pub type mxChar = ::std::os::raw::c_char;
pub type mxLogical = ::std::os::raw::c_uchar;
type size_t = usize;

extern "C" {
    pub fn mexFunctionName() -> *const ::std::os::raw::c_char;
}
extern "C" {
    pub fn mexCallMATLAB(
        nargout: ::std::os::raw::c_int,
        argout: *mut *mut mxArray,
        nargin: ::std::os::raw::c_int,
        argin: *mut *mut mxArray,
        fname: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexCallMATLABWithTrap(
        nargout: ::std::os::raw::c_int,
        argout: *mut *mut mxArray,
        nargin: ::std::os::raw::c_int,
        argin: *mut *mut mxArray,
        fname: *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mexEvalString(s: *const ::std::os::raw::c_char) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexEvalStringWithTrap(s: *const ::std::os::raw::c_char) -> *mut mxArray;
}
extern "C" {
    pub fn mexSetTrapFlag(flag: ::std::os::raw::c_int);
}
extern "C" {
    pub fn mexErrMsgTxt(s: *const ::std::os::raw::c_char);
}
extern "C" {
    pub fn mexErrMsgIdAndTxt(
        id: *const ::std::os::raw::c_char,
        s: *const ::std::os::raw::c_char,
        ...
    );
}
extern "C" {
    pub fn mexWarnMsgTxt(s: *const ::std::os::raw::c_char);
}
extern "C" {
    pub fn mexWarnMsgIdAndTxt(
        id: *const ::std::os::raw::c_char,
        s: *const ::std::os::raw::c_char,
        ...
    );
}
extern "C" {
    pub fn mexPrintf(fmt: *const ::std::os::raw::c_char, ...) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexGetVariable(
        space: *const ::std::os::raw::c_char,
        name: *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mexGetVariablePtr(
        space: *const ::std::os::raw::c_char,
        name: *const ::std::os::raw::c_char,
    ) -> *const mxArray;
}
extern "C" {
    pub fn mexPutVariable(
        space: *const ::std::os::raw::c_char,
        name: *const ::std::os::raw::c_char,
        ptr: *const mxArray,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexGet(handle: f64, property: *const ::std::os::raw::c_char) -> *const mxArray;
}
extern "C" {
    pub fn mexSet(
        handle: f64,
        property: *const ::std::os::raw::c_char,
        val: *mut mxArray,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexMakeArrayPersistent(ptr: *mut mxArray);
}
extern "C" {
    pub fn mexMakeMemoryPersistent(ptr: *mut ::std::os::raw::c_void);
}
extern "C" {
    pub fn mexLock();
}
extern "C" {
    pub fn mexUnlock();
}
extern "C" {
    pub fn mexIsGlobal(ptr: *const mxArray) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexIsLocked() -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mexAtExit(f: ::std::option::Option<unsafe extern "C" fn()>) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxIsFinite(v: f64) -> bool;
}
extern "C" {
    pub fn mxIsInf(v: f64) -> bool;
}
extern "C" {
    pub fn mxIsNaN(v: f64) -> bool;
}
extern "C" {
    pub fn mxGetEps() -> f64;
}
extern "C" {
    pub fn mxGetInf() -> f64;
}
extern "C" {
    pub fn mxGetNaN() -> f64;
}
extern "C" {
    pub fn mxCalloc(n: size_t, size: size_t) -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxMalloc(n: size_t) -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxRealloc(ptr: *mut ::std::os::raw::c_void, size: size_t)
        -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxFree(ptr: *mut ::std::os::raw::c_void);
}
extern "C" {
    pub fn mxCreateCellArray(ndims: mwSize, dims: *const mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateCellMatrix(m: mwSize, n: mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateCharArray(ndims: mwSize, dims: *const mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateCharMatrixFromStrings(
        m: mwSize,
        str_: *mut *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateDoubleMatrix(nr: mwSize, nc: mwSize, flag: mxComplexity) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateDoubleScalar(val: f64) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateLogicalArray(ndims: mwSize, dims: *const mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateLogicalMatrix(m: mwSize, n: mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateLogicalScalar(val: mxLogical) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateNumericArray(
        ndims: mwSize,
        dims: *const mwSize,
        class_id: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateNumericMatrix(
        m: mwSize,
        n: mwSize,
        class_id: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateUninitNumericArray(
        ndims: mwSize,
        dims: *const mwSize,
        class_id: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateUninitNumericMatrix(
        m: mwSize,
        n: mwSize,
        class_id: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateSparse(m: mwSize, n: mwSize, nzmax: mwSize, flag: mxComplexity) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateSparseLogicalMatrix(m: mwSize, n: mwSize, nzmax: mwSize) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateString(str_: *const ::std::os::raw::c_char) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateStructArray(
        ndims: mwSize,
        dims: *const mwSize,
        num_keys: ::std::os::raw::c_int,
        keys: *mut *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxCreateStructMatrix(
        rows: mwSize,
        cols: mwSize,
        num_keys: ::std::os::raw::c_int,
        keys: *mut *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxDuplicateArray(v: *const mxArray) -> *mut mxArray;
}
extern "C" {
    pub fn mxDestroyArray(v: *mut mxArray);
}
extern "C" {
    pub fn mxIsCell(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsChar(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsClass(ptr: *const mxArray, name: *const ::std::os::raw::c_char) -> bool;
}
extern "C" {
    pub fn mxIsComplex(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsDouble(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsFunctionHandle(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsInt16(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsInt32(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsInt64(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsInt8(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsLogical(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsNumeric(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsSingle(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsSparse(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsStruct(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsUint16(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsUint32(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsUint64(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsUint8(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsLogicalScalar(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsLogicalScalarTrue(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsEmpty(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsScalar(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxIsFromGlobalWS(ptr: *const mxArray) -> bool;
}
extern "C" {
    pub fn mxGetM(ptr: *const mxArray) -> size_t;
}
extern "C" {
    pub fn mxGetN(ptr: *const mxArray) -> size_t;
}
extern "C" {
    pub fn mxGetDimensions(ptr: *const mxArray) -> *const mwSize;
}
extern "C" {
    pub fn mxGetNumberOfDimensions(ptr: *const mxArray) -> mwSize;
}
extern "C" {
    pub fn mxGetNumberOfElements(ptr: *const mxArray) -> size_t;
}
extern "C" {
    pub fn mxSetM(ptr: *mut mxArray, M: mwSize);
}
extern "C" {
    pub fn mxSetN(ptr: *mut mxArray, N: mwSize);
}
extern "C" {
    pub fn mxSetDimensions(
        ptr: *mut mxArray,
        dims: *const mwSize,
        ndims: mwSize,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxGetPi(ptr: *const mxArray) -> *mut f64;
}
extern "C" {
    pub fn mxGetPr(ptr: *const mxArray) -> *mut f64;
}
extern "C" {
    pub fn mxGetScalar(ptr: *const mxArray) -> f64;
}
extern "C" {
    pub fn mxGetChars(ptr: *const mxArray) -> *mut mxChar;
}
extern "C" {
    pub fn mxGetLogicals(ptr: *const mxArray) -> *mut mxLogical;
}
extern "C" {
    pub fn mxGetData(ptr: *const mxArray) -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxGetImagData(ptr: *const mxArray) -> *mut ::std::os::raw::c_void;
}
extern "C" {
    pub fn mxSetPr(ptr: *mut mxArray, pr: *mut f64);
}
extern "C" {
    pub fn mxSetPi(ptr: *mut mxArray, pi: *mut f64);
}
extern "C" {
    pub fn mxSetData(ptr: *mut mxArray, data: *mut ::std::os::raw::c_void);
}
extern "C" {
    pub fn mxSetImagData(ptr: *mut mxArray, pi: *mut ::std::os::raw::c_void);
}
extern "C" {
    pub fn mxGetClassID(ptr: *const mxArray) -> mxClassID;
}
extern "C" {
    pub fn mxGetClassName(ptr: *const mxArray) -> *const ::std::os::raw::c_char;
}
extern "C" {
    pub fn mxSetClassName(ptr: *mut mxArray, name: *const ::std::os::raw::c_char);
}
extern "C" {
    pub fn mxGetProperty(
        ptr: *const mxArray,
        idx: mwIndex,
        property_name: *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxSetProperty(
        ptr: *mut mxArray,
        idx: mwIndex,
        property_name: *const ::std::os::raw::c_char,
        property_value: *const mxArray,
    );
}
extern "C" {
    pub fn mxGetCell(ptr: *const mxArray, idx: mwIndex) -> *mut mxArray;
}
extern "C" {
    pub fn mxSetCell(ptr: *mut mxArray, idx: mwIndex, val: *mut mxArray);
}
extern "C" {
    pub fn mxGetIr(ptr: *const mxArray) -> *mut mwIndex;
}
extern "C" {
    pub fn mxGetJc(ptr: *const mxArray) -> *mut mwIndex;
}
extern "C" {
    pub fn mxGetNzmax(ptr: *const mxArray) -> mwSize;
}
extern "C" {
    pub fn mxSetIr(ptr: *mut mxArray, ir: *mut mwIndex);
}
extern "C" {
    pub fn mxSetJc(ptr: *mut mxArray, jc: *mut mwIndex);
}
extern "C" {
    pub fn mxSetNzmax(ptr: *mut mxArray, nzmax: mwSize);
}
extern "C" {
    pub fn mxAddField(
        ptr: *mut mxArray,
        key: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxRemoveField(ptr: *mut mxArray, key_num: ::std::os::raw::c_int);
}
extern "C" {
    pub fn mxGetField(
        ptr: *const mxArray,
        index: mwIndex,
        key: *const ::std::os::raw::c_char,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxGetFieldByNumber(
        ptr: *const mxArray,
        index: mwIndex,
        key_num: ::std::os::raw::c_int,
    ) -> *mut mxArray;
}
extern "C" {
    pub fn mxSetField(
        ptr: *mut mxArray,
        index: mwIndex,
        key: *const ::std::os::raw::c_char,
        val: *mut mxArray,
    );
}
extern "C" {
    pub fn mxSetFieldByNumber(
        ptr: *mut mxArray,
        index: mwIndex,
        key_num: ::std::os::raw::c_int,
        val: *mut mxArray,
    );
}
extern "C" {
    pub fn mxGetNumberOfFields(ptr: *const mxArray) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxGetFieldNameByNumber(
        ptr: *const mxArray,
        key_num: ::std::os::raw::c_int,
    ) -> *const ::std::os::raw::c_char;
}
extern "C" {
    pub fn mxGetFieldNumber(
        ptr: *const mxArray,
        key: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxGetString(
        ptr: *const mxArray,
        buf: *mut ::std::os::raw::c_char,
        buflen: mwSize,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn mxArrayToString(ptr: *const mxArray) -> *mut ::std::os::raw::c_char;
}
extern "C" {
    pub fn mxCalcSingleSubscript(ptr: *const mxArray, nsubs: mwSize, subs: *mut mwIndex)
        -> mwIndex;
}
extern "C" {
    pub fn mxGetElementSize(ptr: *const mxArray) -> size_t;
}
extern "C" {
    pub fn mexFunction(
        nlhs: ::std::os::raw::c_int,
        plhs: *mut *mut mxArray,
        nrhs: ::std::os::raw::c_int,
        prhs: *mut *const mxArray,
    );
}
