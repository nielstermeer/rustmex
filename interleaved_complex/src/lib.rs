#![allow(unused_unsafe)]
use std::slice::{
	from_raw_parts_mut,
	from_raw_parts
};
use core::ffi::c_void;
use core::ops::DerefMut;

use ndarray::{
	Array,
	ArrayViewD,
	ArrayViewMutD,
	Dimension,
};
use num_complex::Complex;

use rustmex_core::{
	mxArray,

	num_struct,
	data_or_dangling,
	data_shape_ok,
	create_uninit_numeric_array,
	Complexity,

	numeric::{NumericArray, ComplexNumericArray, MutNumericArray, MatlabNumber},
	pointers::{MatlabPtr, MutMatlabPtr, MxArray},
	convert::{ToMatlabResult, FromVec, VecLayout, FromMatlabError},
	shim::rustmex_create_uninit_numeric_array,

	MatlabClass,
	MutMatlabClass,
	OwnedMatlabClass,
	NewEmpty,
	TakeData,

	raw::{
		mwSize,
		mxClassID,
	},
};

extern "Rust" {
	fn rustmex_get_interleaved_complex(mx: *const mxArray) -> *mut Complex<c_void>;
	fn rustmex_set_interleaved_complex(mx: *mut mxArray, newdata: *mut Complex<c_void>);
}

num_struct!(
/**
 Complex numeric array, interleaving real and complex parts. Available when the
 `matlab800` backend is used.
 */
InterleavedComplexArray, Complexity::Complex);

macro_rules! data_access {
	($s:ident, $t1:ty, $builder:ident) => {{
		let ptr = data_or_dangling!(unsafe { rustmex_get_interleaved_complex($s.array.deref()) }, $t1 );
		let numel = $s.array.numel();
		unsafe { $builder(ptr, numel) }
	}}
}

impl<'p, T, P> NumericArray<'p> for InterleavedComplexArray<T, P> where
	T: MatlabNumber + 'p,
	P: MatlabPtr + 'p,
{
	type Data = &'p [Complex<T>];
	fn data(&self) -> Self::Data {
		data_access!(self, *const Complex<T>, from_raw_parts)
	}
}

impl<'p, T, P> MutNumericArray<'p> for InterleavedComplexArray<T, P> where
	T: MatlabNumber + 'p,
	P: MutMatlabPtr + 'p,
{
	type MutData = &'p mut [Complex<T>];
	fn mut_data(&mut self) -> Self::MutData {
		data_access!(self, *mut Complex<T>, from_raw_parts_mut)
	}
}

impl<T, P> TakeData<P> for InterleavedComplexArray<T, P> where
	T: MatlabNumber,
	P: MutMatlabPtr,
{
	type OwnedData = Box<[Complex<T>]>;
	fn take_data(&mut self) -> Self::OwnedData {

		// use mut_data to get the data slice, since that method implements that
		// correctly
		let data = self.mut_data();

		// SAFETY: Omg this is such a mess
		// Technically, Vec wants to know the _original_ size of the allocation,
		// which is something we can't get out of matlab. So there are two
		// options: either do a complicated and expensive realloc, or (possibly)
		// lie to Vec and Box about the size of the allocation.
		//
		// For now, we're doing the latter. The Matlab allocator does not care
		// about the allocated size, after all, it only cares about the pointer
		// it gets (it probably tracks the size internally).
		let data = unsafe { Vec::from_raw_parts(data.as_mut_ptr(), data.len(), data.len()) }
			.into_boxed_slice();

		// SAFETY: Set the data pointer of the array to NULL, so we don't have an
		// aliased pointer lying around.
		unsafe { rustmex_set_interleaved_complex(self.deref_mut(), core::ptr::null_mut()) };

		data
	}
}

impl<'p, T, P> ComplexNumericArray<'p> for InterleavedComplexArray<T, P> where
	T: MatlabNumber + 'p,
	P: MatlabPtr + 'p,
{
	type CDT = T;
}

impl<T: MatlabNumber> InterleavedComplexArray<T, MxArray> {
	pub fn new(data: Box<[Complex<T>]>, shape: &[usize]) -> ToMatlabResult<Self, Box<[Complex<T>]>> {
		data_shape_ok!(data, shape);
		let mx = create_uninit_numeric_array!(shape, T, Complexity::Complex);
		unsafe { rustmex_set_interleaved_complex(mx, Box::into_raw(data) as *mut Complex<c_void>); }
		Ok(Self::construct(unsafe {MxArray::assume_responsibility_ptr(mx)}))
	}
}

impl<'a, T> From<InterleavedComplexArray<T, &'a mxArray>> for ArrayViewD<'a, Complex<T>> where
	T: MatlabNumber
{
	fn from(num: InterleavedComplexArray<T, &'a mxArray>) -> Self {
		(&num).into()
	}
}

impl<'a, 'b, T, P> From<&InterleavedComplexArray<T, P>> for ArrayViewD<'a, Complex<T>> where
	T: MatlabNumber,
	P: MatlabPtr + 'a,
	'b: 'a
{
	fn from(num: &InterleavedComplexArray<T, P>) -> Self {
		let data = num.data();
		let dims = num.dimensions();
		rustmex_core::from_num_to_ndarray!(data, dims)
	}
}

impl<'a, T> From<InterleavedComplexArray<T, &'a mut mxArray>> for ArrayViewMutD<'a, Complex<T>> where
	T: MatlabNumber
{
	fn from(mut num: InterleavedComplexArray<T, &'a mut mxArray>) -> Self {
		let data = num.mut_data();
		let dims = num.dimensions();
		rustmex_core::from_num_to_ndarray!(data, dims)
	}
}

impl<'a, 'b, T, P> From<&'b mut InterleavedComplexArray<T, P>> for ArrayViewMutD<'a, Complex<T>> where
	T: MatlabNumber,
	P: MutMatlabPtr + 'a,
	'b: 'a,
{
	fn from(num: &mut InterleavedComplexArray<T, P>) -> Self {
		let data = num.mut_data();
		let dims = num.dimensions();
		rustmex_core::from_num_to_ndarray!(data, dims)
	}
}

impl<'a, T, D> From<Array<Complex<T>, D>> for InterleavedComplexArray<T, MxArray> where
	T: MatlabNumber,
	D: Dimension,
{
	fn from(mut arr: Array<Complex<T>, D>) -> Self {
		rustmex_core::from_ndarray_to_num!(arr)
	}
}

impl<T> From<[Complex<T>;0]> for InterleavedComplexArray<T, MxArray> where T: MatlabNumber {
	fn from(_empty: [Complex<T>;0]) -> Self {
		Self::new_empty()
	}
}

impl<T> From<Complex<T>> for InterleavedComplexArray<T, MxArray> where T: MatlabNumber {
	fn from(cn: Complex<T>) -> Self {
		Self::new(Box::from([cn]), &[1,1]).expect("scalar should fit in 1x1")
	}
}

impl<T> FromVec<Complex<T>> for InterleavedComplexArray<T, MxArray> where T: MatlabNumber {
	fn from_boxed_slice<L: VecLayout>(b: Box<[Complex<T>]>, l: L) -> Self {
		let len = b.len();
		Self::new(b, l.layout(len).as_ref()).expect("Conversion from Vec-ish type should be infallible")
	}
}
