/*!
 * Signatures of shim functions between Rustmex and the used backend.
 *
 * Rustmex supports several backends, namely:
 *  - matlab800, for versions of matlab which store the real and imaginary parts
 *  interleaved (_i.e._ modern versions of matlab)
 *  - matlab700, for older versions of matlab which store the real and imaginary parts of
 *  complex arrays separately (older versions of matlab)
 *  - octave, for GNU/Octave
 *
 *  Current versions of Rustmex don't assume anything in particular about the backend
 *  (hence the helper crates too which specialise for the different backends). Moreover,
 *  the different backends use different names for each API. In the C api, this is
 *  papered over with macro's, but Rustmex uses shim functions. This module defines the
 *  signatures of the shim functions, which the backend shim crates then implement.
 */

#![allow(non_camel_case_types)]
use ::std::os::raw::{
	c_void,
	c_int,
	c_char,
};

use crate::raw::*;
use crate::mxArray;

type mxClassID = u32;
type mxComplexity = u32;

extern "Rust" {
pub fn rustmex_malloc(n: usize) -> *mut c_void;
pub fn rustmex_free(ptr: *mut c_void);
pub fn rustmex_make_memory_persistent(ptr: *mut c_void);
pub fn rustmex_get_dimensions(mx: *const mxArray) -> *const usize;
pub fn rustmex_get_number_of_dimensions(mx: *const mxArray) -> usize;
pub fn rustmex_numel(mx: *const mxArray) -> usize;
pub fn rustmex_get_class_id(mx: *const mxArray) -> mxClassID;
pub fn rustmex_is_complex(mx: *const mxArray) -> bool;
pub fn rustmex_is_sparse(mx: *const mxArray) -> bool;
pub fn rustmex_is_struct(mx: *const mxArray) -> bool;
pub fn rustmex_is_class(mx: *const mxArray, name: *const c_char) -> bool;
pub fn rustmex_duplicate_array(mx: *const mxArray) -> *mut mxArray;
pub fn rustmex_destroy_array(mx: *mut mxArray);
pub fn rustmex_create_uninit_numeric_array(
        ndim: usize,
        dims: *const usize,
        classid: mxClassID,
        flag: mxComplexity,
    ) -> *mut mxArray;
pub fn rustmex_set_dimensions(mx: *mut mxArray, dims: *const usize, ndims: usize) -> i32;
pub fn rustmex_get_data(mx: *const mxArray) -> *mut c_void;
pub fn rustmex_set_data(mx: *mut mxArray, newdata: *mut c_void);
pub fn rustmex_get_number_of_fields(mx: *const mxArray) -> c_int;
pub fn rustmex_get_field_number(mx: *const mxArray, name: *const c_char) -> c_int;
pub fn rustmex_get_field_name_by_number(mx: *const mxArray, num: c_int) -> *const c_char;
pub fn rustmex_get_field_by_number(mx: *const mxArray, linidx: mwIndex, fieldnum: c_int) -> *mut mxArray;
pub fn rustmex_set_field_by_number(mx: *mut mxArray, linidx: mwIndex, fieldnum: c_int, data: *mut mxArray);
pub fn rustmex_add_field(mx: *mut mxArray, fieldname: *const c_char) -> c_int;
pub fn rustmex_remove_field(mx: *mut mxArray, fieldnum: c_int);
pub fn rustmex_create_struct_array(ndims: mwSize, dims: *const mwSize, nfields: c_int, fieldnames: *mut *const c_char) -> *mut mxArray;
pub fn rustmex_get_property(mx: *const mxArray, linidx: mwIndex, propname: *const c_char) -> *mut mxArray;
pub fn rustmex_set_property(mx: *mut mxArray, linidx: mwIndex, propname: *const c_char, value: *const mxArray);
pub fn rustmex_get_cell(mx: *const mxArray, i: mwIndex) -> *mut mxArray;
pub fn rustmex_set_cell(mx: *mut mxArray, i: mwIndex, val: *mut mxArray);
pub fn rustmex_create_cell_array(ndims: mwSize, dims: *const mwSize) -> *mut mxArray;
pub fn rustmex_get_variable_ptr(workspace: *const c_char, name: *const c_char) -> *const mxArray;
pub fn rustmex_put_variable(workspace: *const c_char, name: *const c_char, val: *mut mxArray) -> c_int;
pub fn rustmex_call_matlab(nlhs: c_int, lhs: *mut *mut mxArray, nrhs: c_int, rhs: *mut *mut mxArray, fname: *const c_char) -> c_int;
pub fn rustmex_call_matlab_with_trap(nlhs: c_int, lhs: *mut *mut mxArray, nrhs: c_int, rhs: *mut *mut mxArray, fname: *const c_char) -> *mut mxArray;
pub fn rustmex_warn_id_and_txt(id: *const c_char, msg: *const c_char);
pub fn rustmex_err_id_and_txt(id: *const c_char, msg: *const c_char);
}
