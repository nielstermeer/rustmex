/*!
 * Numeric array and numeric data handling
 */

use std::borrow::Cow;
use num_complex::Complex;
use crate::classid::ClassID;

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum ComplexData<I, S> {
	Interleaved(I),
	Separated(S)
}

type IComplex<'a, T> = &'a [Complex<T>];
type SComplex<'a, T> = Complex<&'a [T]>;
type CData<'a, T> = ComplexData<IComplex<'a, T>, SComplex<'a, T>>;

impl<I, S> ComplexData<I, S> {
	pub fn assume_interleaved(self) -> I {
		use ComplexData::*;
		match self {
			Interleaved(i) => i,
			Separated(_) => panic!("Expected interleaved, have separated")
		}
	}
	pub fn assume_separated(self) -> S {
		use ComplexData::*;
		match self {
			Separated(s) => s,
			Interleaved(_) => panic!("Expected separated, have interleaved")
		}
	}
}

impl<'a, T> From<&'a [Complex<T>]> for ComplexData<&'a [Complex<T>], Complex<&'a [T]>> {
	fn from(source: &'a [Complex<T>]) -> Self {
		Self::Interleaved(source)
	}
}

impl<'a, T> From<Complex<&'a [T]>> for ComplexData<&'a [Complex<T>], Complex<&'a [T]>> {
	fn from(source: Complex<&'a [T]>) -> Self {
		Self::Separated(source)
	}
}

impl<'a, T> From<ComplexData<IComplex<'a, T>, SComplex<'a, T>>> for Cow<'a, [Complex<T>]>
	where T: Copy,
{
	fn from(cd: ComplexData<IComplex<'a, T>, SComplex<'a, T>>) -> Self {
		match cd {
			ComplexData::Interleaved(i) => Self::from(i),
			ComplexData::Separated(s) => {
				let v: Vec<_> = s.re.iter().zip(s.im.iter())
					.map(|(&re, &im)| Complex {
						re,
						im
					})
					.collect();

				Self::from(v)
			}
		}
	}
}

/// Trait which links a mutable borrow of something to its shared counterpart. Meant to
/// be basically `&*T` as a trait, but also for more complex types.
pub trait AsShared<T> where T:?Sized {
	fn as_shared(x: Self) -> T;
}

impl<'t, T> AsShared<&'t T> for &'t mut T where T: ?Sized {
	fn as_shared(x: Self) -> &'t T {
		&*x
	}
}

impl<'t, T> AsShared<Complex<&'t [T]>> for Complex<&'t mut [T]> {
	fn as_shared(x: Self) -> Complex<&'t [T]> {
		use crate::mappable::Mappable;
		x.map(|t| &*t)
	}
}

pub trait NumericArray<'p> {
	type Data: ?Sized + 'p;
	fn data(&self) -> Self::Data;
}

pub trait MutNumericArray<'p>: NumericArray<'p> {
	type MutData: ?Sized + 'p + AsShared<Self::Data>;
	fn mut_data(&mut self) -> Self::MutData;
}

pub trait ComplexNumericArray<'p>: NumericArray<'p> where
	<Self as NumericArray<'p>>::Data: Sized,
	CData<'p, Self::CDT>: From<<Self as NumericArray<'p>>::Data>,
{
	type CDT: MatlabNumber + 'p;
	fn complex_data(&self) -> CData<'p, Self::CDT> {
		let data = self.data();
		CData::from(data)
	}
}

pub trait MatlabNumber: Copy + std::fmt::Debug {
	fn class_id() -> ClassID;
}

macro_rules! impl_ml_number {
	($name:ident, $cd:expr) => {
		impl MatlabNumber for $name {
			fn class_id() -> ClassID {
				$cd
			}
		}
	}
}

impl_ml_number!(i8, ClassID::I8);
impl_ml_number!(u8, ClassID::U8);
impl_ml_number!(i16, ClassID::I16);
impl_ml_number!(u16, ClassID::U16);
impl_ml_number!(i32, ClassID::I32);
impl_ml_number!(u32, ClassID::U32);
impl_ml_number!(i64, ClassID::I64);
impl_ml_number!(u64, ClassID::U64);
impl_ml_number!(f32, ClassID::Single);
impl_ml_number!(f64, ClassID::Double);
impl_ml_number!(bool, ClassID::Logical);

/**
 * Write out the definition for a numerical structure. Internal Rustmex tool.
 *
 * It also implements [`Deref`](core::ops::Deref) and [`DerefMut`](core::ops::DerefMut)
 * to dereference it to an [`mxArray`](crate::mxArray), and the `new_empty` method, to
 * create an empty numerical struct.
 *
 * It also derives implementations for [`Clone`], [`Copy`] (which only apply if the inner
 * type `P` is `Clone` or `Copy`, [`Debug`], [`Eq`], and [`PartialEq`].
 */
#[doc(hidden)]
#[macro_export]
macro_rules! num_struct {
	(#[$doc:meta] $name:ident, $complexity:expr) => {
		#[$doc]
		#[derive(Clone, Copy, Debug, Eq, PartialEq)]
		#[repr(transparent)]
		pub struct $name<T, P> {
			array: P,
			dtype: ::std::marker::PhantomData<T>,
		}

		impl<T, P> $name<T, P> {
			fn construct(array: P) -> Self {
				Self { array, dtype: ::std::marker::PhantomData }
			}
		}

		impl<T, P> std::ops::Deref for $name<T, P> where P: MatlabPtr {
			type Target = mxArray;

			fn deref(&self) -> &Self::Target {
				&self.array
			}
		}

		impl<T, P> std::ops::DerefMut for $name<T, P> where P: MutMatlabPtr {
			fn deref_mut(&mut self) -> &mut Self::Target {
				&mut self.array
			}
		}

		impl<T, P> MatlabClass<P> for $name<T, P> where
			T: MatlabNumber,
			P: MatlabPtr,
		{
			fn from_mx_array(mx: P) -> Result<Self, FromMatlabError<P>> {
				use ::rustmex_core::convert::FromMatlabError;
				if mx.raw_class_id() != T::class_id() {
					return Err(FromMatlabError::new_badclass(mx))
				}

				if mx.complexity() != $complexity {
					return Err(FromMatlabError::new_badcomplexity(mx))
				}

				if mx.is_sparse() {
					return Err(FromMatlabError::new_badsparsity(mx))
				}

				Ok(Self::construct(mx))
			}

			fn into_inner(self) -> P {
				self.array
			}

			fn inner(&self) -> &P {
				&self.array
			}

			type Owned = $name<T, MxArray>;
			fn duplicate(&self) -> Self::Owned {
				let array = self.array.duplicate();
				Self::Owned::construct(array)
			}
		}

		impl<T, P> MutMatlabClass<P> for $name<T, P> where
			T: MatlabNumber,
			P: MutMatlabPtr,
		{
			type AsBorrowed<'a> = $name<T, &'a mxArray> where P: 'a, T: 'a;
			fn as_borrowed<'a>(&'a self) -> Self::AsBorrowed<'a> {
				let array = self.array.deref();
				Self::AsBorrowed::construct(array)
			}

			fn inner_mut(&mut self) -> &mut P {
				&mut self.array
			}
		}

		impl<T> OwnedMatlabClass for $name<T, MxArray> where T: MatlabNumber {
			type AsMutable<'a> = $name<T, &'a mut mxArray> where Self: 'a;
			fn as_mutable<'a>(&'a mut self) -> Self::AsMutable<'a> {
				Self::AsMutable::construct(self.array.deref_mut())
			}
		}

		impl<T> NewEmpty for $name<T, MxArray> where T: MatlabNumber {
			fn new_empty() -> Self {
				const EMPTY_SIZE: [mwSize; 2] = [0, 0];

				// SAFETY: The empty_size must be a proper mwSize; don't
				// accidentally pass i32's.
				//
				// Furthermore, if a MEX function receives an empty
				// array, then the pointer returned from `mxGetData` is
				// NULL. This implies that Matlab initializes it this
				// way, so we don't have explicitly pass in a null
				// pointer via `mxSetData`.
				let mx = unsafe {
					rustmex_create_uninit_numeric_array(
						EMPTY_SIZE.len() as mwSize,
						EMPTY_SIZE.as_ptr(),
						T::class_id() as mxClassID,
						($complexity).into())
				};

				Self::construct(
					unsafe { MxArray::assume_responsibility_ptr(mx) }
				)
			}
		}
	}
}

/**
 * Convert a numeric mxArray to a numeric NDarray. Internal Rustmex tool.
 */
#[doc(hidden)]
#[macro_export]
macro_rules! from_num_to_ndarray {
	($data:ident, $dims:ident) => {{ rustmex_core::from_num_to_ndarray!($data, $dims, Self) }};
	($data:ident, $dims:ident, $t:ty) => {{
		use ndarray::{
			IxDyn,
			ShapeBuilder,
		};
		// TL; DR: Matlab is _very_ dumb.
		//
		// It seems like it uses different layouts depending on how many
		// dimensions the matrix has.
		//
		// For example, for a NxM matrix, it uses row major. This is a
		// simple fix, just reverse the axes and we have a normal layout.
		//
		// But once MxNxL is introduced, this breaks down. The fix seems
		// to be that we still reverse the axis, but then swap the last
		// two so it makes sense again.
		//
		// In other words:
		// Matlab : row, column, plane, block <- Also fortran layout
		// reverse: block, plane, column, row
		// swap   : block, plane, row, column <- Also C/Rust/NdArray layout
		let mut v = <$t>::from_shape(IxDyn($dims).f(), $data)
			.expect("MATLAB should provide corectly shaped data")
			.reversed_axes();
		v.swap_axes($dims.len() - 2, $dims.len() - 1);
		v
	}}
}

/**
 * Convert a numeric NDarray to a numeric MxArray. Internal Rustmex tool.
 */
#[doc(hidden)]
#[macro_export]
macro_rules! from_ndarray_to_num {
	/*
	 * Shuffle the array's dimensions around so that the layout seen from Rust
	 * matches what will be seen in Matlab. It undoes the shuffling performed by []
	 */
	(@shuffle $arr:ident) => {{
		let num_axes = $arr.shape().len();

		if num_axes >= 2 {
			$arr.swap_axes(num_axes - 2, num_axes - 1);
		}

		let reshaped = if let Ok(v) = $arr.reversed_axes()
			.try_into_owned_nocopy()
		{
			v
		} else {
			panic!("Reversing axing strides should not cause a copy");
		};
		reshaped
	}};
	($arr:ident) => {{
		let reshaped = rustmex_core::from_ndarray_to_num!(@shuffle $arr);

		// this dance with the Dimension and the shape is required because after
		// this step we convert the Array into a raw vector, so any reference
		// into it regarding the shape would no longer be valid.
		let dim = reshaped.raw_dim();
		let shape = dim.as_array_view().to_slice().unwrap();

		// There is as of yet no way to directly extract a heap allocated slice
		// out of NDarray, so we explicitly go via Vec to Box instead.
		let data = reshaped
			.into_raw_vec()
			.into_boxed_slice();

		Self::new(data, shape).expect("NDarray should give consistent data")
	}}
}
