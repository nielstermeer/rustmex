/*!
 * Module defining how Matlab pointers behave
 */

use std::ops::{Deref, DerefMut, Drop};
use std::clone::Clone;
use std::borrow::{ToOwned, Borrow};
use std::convert::AsRef;
use std::ptr::NonNull;

use crate::{mxArray, MatlabClass};

use crate::shim::{
	rustmex_duplicate_array,
	rustmex_destroy_array,
};

/**
 * A Matlab Pointer is a marker trait, used to be generic over pointers which can be used
 * across API boundaries with Matlab. Two of these are already implemented (Matlab
 * returns an opaque pointer to objects, so these can just be used as-is as references),
 * which leaves the third — the owned type — to be implemented. _See_ [`MxArray`].
 */
pub trait MatlabPtr: Deref<Target = mxArray> {

	/**
	 * A [`MatlabPtr`] is an untyped pointer; it does not know what the type of the
	 * data it holds is. However, Rustmex provides types which represent that
	 * information. If the `MatlabPtr` holds one of those data types, "upcast" the
	 * pointer to that type and return it.
	 */
	// TODO: Rewrite so that it doesn't eat the mxArray, especially not when its an
	// owned pointer
	fn is_a<W>(self) -> Result<W, crate::convert::FromMatlabError<Self>> where
		W: MatlabClass<Self>,
		Self: Sized,
	{
		W::from_mx_array(self)
	}

	/**
	 * Return the type name of the `MatlabPtr`. It is in the same vein as
	 * [`std::any::type_name`], but it yields a shorter, not-fully-qualified name. It
	 * is intended to be used primarily for building the [`Debug`] implementations of
	 * [`MatlabClass`]es
	 */
	fn type_name() -> &'static str;
}

impl<'a> MatlabPtr for &'a mut mxArray {
	fn type_name() -> &'static str {
		"&mut mxArray"
	}
}

impl<'a> MatlabPtr for &'a mxArray {
	fn type_name() -> &'static str {
		"&mxArray"
	}
}

impl MatlabPtr for MxArray {
	fn type_name() -> &'static str {
		"MxArray"
	}
}

pub trait MutMatlabPtr: DerefMut<Target = mxArray> + MatlabPtr {}
impl<T> MutMatlabPtr for T where T: DerefMut<Target = mxArray> + MatlabPtr {}

/**
 * The owned variant of an mxArray. Some of Matlab's mex functions specify that the
 * caller is responsible for deallocating the object when it is done with it, unless the
 * object in question is returned to Matlab. This cannot be expressed with just (mutable)
 * references, so this type implements the owned type.
 *
 * It is basically a wrapper around a mutable reference to an mxArray, but it implements
 * the drop trait, so when it is dropped it calls `mxDestroyArray`. It implements
 * [`Deref`] and [`DerefMut`], so it can also be used where-ever a (mutable) reference to
 * an mxArray is expected.
 */
#[repr(transparent)]
#[derive(Debug)]
pub struct MxArray(NonNull<mxArray>);

impl Drop for MxArray {
	fn drop(&mut self) {
		unsafe { rustmex_destroy_array(self.0.as_mut()) };
	}
}

impl Deref for MxArray {
	type Target = mxArray;

	fn deref(&self) -> &Self::Target {
		unsafe { self.0.as_ref() }
	}
}

impl DerefMut for MxArray {
	fn deref_mut(&mut self) -> &mut Self::Target {
		unsafe { self.0.as_mut() }
	}
}

impl mxArray {
	/**
	 * Create a deep copy of the array, and return as an owned type. However,
	 * consider using [`MatlabClass::duplicate`] instead if you are already working
	 * with [`MatlabClass`]es.
	 */
	pub fn duplicate(&self) -> MxArray {
		let ptr = unsafe { rustmex_duplicate_array(self) };
		if ptr.is_null() {
			panic!("OOM");
		}
		unsafe { MxArray::assume_responsibility(&mut *ptr ) }
	}
}

impl Clone for MxArray {
	fn clone(&self) -> Self {
		self.duplicate()
	}
}

impl Borrow<mxArray> for MxArray {
	fn borrow(&self) -> &mxArray {
		self.deref()
	}
}

impl ToOwned for mxArray {
	type Owned = MxArray;

	fn to_owned(&self) -> Self::Owned {
		self.duplicate()
	}
}

impl AsRef<mxArray> for mxArray {
	fn as_ref(&self) -> &mxArray {
		self
	}
}

impl AsRef<mxArray> for MxArray {
	fn as_ref(&self) -> &mxArray {
		self.deref()
	}

}

impl MxArray {
	// NOTE: I'm not yet sure whether accepting and returning references from the
	// responsibility handling methods is the best idea. Maybe accepting/returning
	// (NonNull) pointers is better?

	/**
	 * Create an MxArray by assuming responsibility for freeing an mxArray. Unless
	 * you have allocated an mxArray yourself, you do not need this.
	 */
	pub unsafe fn assume_responsibility(mx: &'static mut mxArray) -> Self {
		Self(NonNull::new_unchecked(mx as *mut mxArray))
	}

	/// _See_ [`MxArray::assume_responsibility`].
	pub unsafe fn assume_responsibility_ptr(mx: *mut mxArray) -> Self {
		Self(NonNull::new(mx).expect("Non-null pointer"))
	}

	/**
	 * Transfer responsibility for deallocating this mxArray back to Matlab. Unless
	 * you are manually calling raw matlab functions, you do not need this.
	 */
	pub unsafe fn transfer_responsibility(o: Self) -> &'static mut mxArray {
		&mut *Self::transfer_responsibility_ptr(o)
	}

	/// _See_ [`MxArray::transfer_responsibility`].
	pub unsafe fn transfer_responsibility_ptr(o: Self) -> *mut mxArray {
		let ptr = o.0.as_ptr();
		std::mem::forget(o);
		ptr
	}
}
