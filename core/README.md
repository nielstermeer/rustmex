# Rustmex Core

The core crate of the Rustmex family of crates, used to create ergonomic bindings to
Matlab's and Octave's MEX APIs

You should not need to depend directly on this crate in your project. You are probably
looking for the 'Rustmex' crate instead.
